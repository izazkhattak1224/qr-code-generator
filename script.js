const qrText = document.getElementById('qr-text');
const sizes = document.getElementById('sizes');
const generateBtn = document.getElementById('generateBtn');
const downloadBtn = document.getElementById('downloadBtn');
const qrContainer = document.querySelector('.qr-body');
const errorMessage = document.getElementById('error-message');

let size = sizes.value;

generateBtn.addEventListener('click', (e) => {
    e.preventDefault();
    checkInputAndGenerate();
});

sizes.addEventListener('change', (e) => {
    size = e.target.value;
    checkInputAndGenerate();
});

downloadBtn.addEventListener('click', () => {
    const img = document.querySelector('.qr-body img') || document.querySelector('canvas');
    if (img) {
        downloadBtn.setAttribute("href", img.toDataURL());
    }
});

function checkInputAndGenerate() {
    if (qrText.value.trim()) {
        errorMessage.classList.add('hidden');
        generateQRCode();
    } else {
        errorMessage.classList.remove('hidden');
    }
}

function generateQRCode() {
    qrContainer.innerHTML = "";
    new QRCode(qrContainer, {
        text: qrText.value,
        height: size,
        width: size,
        colorLight: "#fff",
        colorDark: "#000",
    });
}
